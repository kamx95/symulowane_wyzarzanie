#pragma once
#include <memory>

class Problem
{
public:
	typedef std::unique_ptr<Problem> Ptr;
	Problem() = default;
	virtual ~Problem() = default;
	virtual Problem::Ptr utworz_podobne() = 0;
	inline double podaj_koszt() { return koszt; }

protected:
	double koszt;
};