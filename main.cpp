#include <iostream>
#include "Komiwojazer_1.h"
#include "Komiwojazer_2.h"
#include "Obrabiarka.h"
#include "Wyzarzanie.h"
#include "Szeregowanie.h"


int main()
{
	std::vector<int> wyniki;
	Wyzarzanie maszyna(2000, 0.9995);

	std::cout << "Co chcesz policzyc:" << std::endl <<
		"1. Komiwojazer dla 1 pojazdu" << std::endl <<
		"2. Komiwojazer dla 2 pojazdow" << std::endl <<
		"3. Obrabiarki" << std::endl <<
		"4. Szeregowanie" << std::endl;

	Problem::Ptr problem;
	std::string filename;
	int wybor;

	std::cin >> wybor;
	switch (wybor)
	{
	case 1: 
		problem = std::make_unique<Komiwojazer_1>("wej.txt");
		break;
	case 2: 
		problem = std::make_unique<Komiwojazer_2>("wej.txt");
		break;
	case 3: 
		std::cout << "1-9" << std::endl;
		char wybor2;
		std::cin >> wybor2;
		filename.assign("data/NEH");
		filename.push_back(wybor2);
		filename.insert(9, ".DAT");
		problem = std::make_unique<Obrabiarka>(filename);
		break;
	case 4:
		problem = std::make_unique<Szeregowanie>("szereg2.txt");
		break;
	default: 
		wybor = -1;
		break;
	}

	if (wybor != -1)
	{
		auto rozwiazanie = maszyna.run(problem.get());
		std::cout << rozwiazanie->podaj_koszt() << std::endl;
	}
	
	return 0;
}