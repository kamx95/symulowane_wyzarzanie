#pragma once
#include "Problem.h"
#include <vector>
#include <random>
#include <fstream>

class Obrabiarka :
	public Problem
{
public:
	Obrabiarka(const Obrabiarka & p);
	Obrabiarka(const std::string & filename);
	virtual ~Obrabiarka() override = default;
	virtual Problem::Ptr utworz_podobne() override;
private:
	typedef std::vector<std::vector<int>> int_2d_vec;
	double ustal_koszt();
	void mutuj_rozwiazanie();
	std::random_device maszyna_losujaca;
	int_2d_vec rozwiazanie;
	int ilosc_maszyn;
	int ilosc_zadan;
};

