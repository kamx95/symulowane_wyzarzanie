#pragma once
#include "Problem.h"
#include <vector>
#include <random>
#include <fstream>

class Komiwojazer_1 :
	public Problem
{
public:
	Komiwojazer_1(const Komiwojazer_1 & p);
	Komiwojazer_1(const std::string & filename);
	virtual ~Komiwojazer_1() override = default;
	virtual Problem::Ptr utworz_podobne() override;
	
private:
	typedef std::vector<std::vector<int>> int_2d_vec;
	double ustal_koszt();
	void mutuj_rozwiazanie();
	std::random_device maszyna_losujaca;
	std::vector<int> rozwiazanie;
	std::shared_ptr<int_2d_vec> mapa_kosztow;
};

