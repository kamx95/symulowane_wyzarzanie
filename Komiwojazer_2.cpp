#include "Komiwojazer_2.h"
#include <algorithm>
#include <limits>

Komiwojazer_2::Komiwojazer_2(const Komiwojazer_2 & p):
rozwiazanie(p.rozwiazanie), mapa_kosztow(p.mapa_kosztow)
{
}

Komiwojazer_2::Komiwojazer_2(const std::string &filename)
{
	int n;
	std::fstream wejscie(filename, std::ios_base::in);
	wejscie >> n;

	mapa_kosztow = std::make_shared<int_2d_vec>(n);
	
	for (auto &vec : *mapa_kosztow)
	{
		vec.resize(n);
		for (auto &val : vec)
		{
			wejscie >> val;
		}
	}
	rozwiazanie = { { 0,1,2,3,4,5,6,7,8,0 } ,{ 0 } };
	std::shuffle(rozwiazanie[0].begin() + 1, rozwiazanie[0].end() - 1,
		std::mt19937(maszyna_losujaca()));

	auto it_begin = rozwiazanie[0].begin() + (maszyna_losujaca() % (rozwiazanie[0].size() - 1)) + 1;
	auto it_end = rozwiazanie[0].end();
	rozwiazanie[1].insert(rozwiazanie[1].end(), it_begin, it_end);
	rozwiazanie[0].erase(it_begin, it_end);
	rozwiazanie[0].push_back(0);

	koszt = ustal_koszt();
}

void Komiwojazer_2::mutuj_rozwiazanie()
{
	size_t dlugosc = rozwiazanie[0].size() + rozwiazanie[1].size();
	size_t index_a2 = (maszyna_losujaca() % (dlugosc - 4)) + 1; 
	size_t index_a1 = index_a2 < rozwiazanie[0].size() - 1? 0 : 1;
	index_a2 = index_a1 ? index_a2 - rozwiazanie[0].size() + 2 : index_a2;

	auto temp = rozwiazanie[index_a1][index_a2];
	rozwiazanie[index_a1].erase(rozwiazanie[index_a1].begin() + index_a2);

	size_t index_b1 = maszyna_losujaca() % 2;
	size_t index_b2 = maszyna_losujaca() % (rozwiazanie[index_b1].size() - 1) + 1;

	rozwiazanie[index_b1].insert(rozwiazanie[index_b1].begin() + index_b2, temp);

	koszt = ustal_koszt();
}

Problem::Ptr Komiwojazer_2::utworz_podobne()
{
	auto tmp = std::make_unique<Komiwojazer_2>(*this);
	tmp->mutuj_rozwiazanie();
	return tmp;
}

double Komiwojazer_2::ustal_koszt()
{
	if (!rozwiazanie.empty() && !mapa_kosztow->empty())
	{
		double tmp1 = 0, tmp2 = 0;

		for (auto it = rozwiazanie[0].begin(); it < rozwiazanie[0].end() - 1; it++)
			tmp1 += (*mapa_kosztow)[*it][it[1]];

		for (auto it = rozwiazanie[1].begin(); it < rozwiazanie[1].end() - 1; it++)
			tmp2 += (*mapa_kosztow)[*it][it[1]];

		return std::max(tmp1, tmp2);
	}
	return std::numeric_limits<double>::max();
}
