#include "Komiwojazer_1.h"
#include <algorithm>
#include <limits>

Komiwojazer_1::Komiwojazer_1(const Komiwojazer_1 & p):
rozwiazanie(p.rozwiazanie), mapa_kosztow(p.mapa_kosztow)
{
}

Komiwojazer_1::Komiwojazer_1(const std::string &filename)
{
	int n;
	std::fstream wejscie(filename, std::ios_base::in);
	wejscie >> n;

	mapa_kosztow = std::make_shared<int_2d_vec>(n);

	for (auto &vec : *mapa_kosztow)
	{
		vec.resize(n);
		for (auto &val : vec)
		{
			wejscie >> val;
		}
	}
	rozwiazanie = { 0,1,2,3,4,5,6,7,8,0 };
	std::shuffle(rozwiazanie.begin() + 1, rozwiazanie.end() - 1,
		std::mt19937(maszyna_losujaca()));
	koszt = ustal_koszt();
}

void Komiwojazer_1::mutuj_rozwiazanie()
{
	unsigned index_a = (maszyna_losujaca() % (rozwiazanie.size() - 2)) + 1;
	unsigned index_b = 0;
	do
	{
		index_b = (maszyna_losujaca() % (rozwiazanie.size() - 2)) + 1;
	} while (index_a == index_b);
	std::swap(rozwiazanie[index_a], rozwiazanie[index_b]);
	koszt = ustal_koszt();
}

Problem::Ptr Komiwojazer_1::utworz_podobne()
{
	auto tmp = std::make_unique<Komiwojazer_1>(*this);
	tmp->mutuj_rozwiazanie();
	return tmp;
}

double Komiwojazer_1::ustal_koszt()
{
	if (!rozwiazanie.empty() && !mapa_kosztow->empty())
	{
		long tmp = 0;
		for (auto it = rozwiazanie.begin(); it < rozwiazanie.end() - 1; it++)
			tmp += (*mapa_kosztow)[*it][it[1]];
		return tmp;
	}
	return std::numeric_limits<double>::max();
}
