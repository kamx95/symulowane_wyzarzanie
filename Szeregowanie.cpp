#include "Szeregowanie.h"

Szeregowanie::Szeregowanie(const std::string &filename)
{
	std::fstream wejscie(filename, std::ios_base::in);
	wejscie >> ilosc_maszyn;

	lista_zadan = std::make_shared<vec_pair_of_int>();
	int czas_trwania, zasoby, i = 0;
	while (!wejscie.eof())
	{
		wejscie >> czas_trwania;
		wejscie >> zasoby;
		lista_zadan->push_back(std::make_pair(czas_trwania, zasoby));
		kolejnosc_zadan.push_back(i++);
	}
	
	std::mt19937 g(maszyna_losujaca());
	std::shuffle(kolejnosc_zadan.begin(), kolejnosc_zadan.end(), g);
	ustal_koszt();
}

Szeregowanie::Szeregowanie(const Szeregowanie &p) :
	lista_zadan(p.lista_zadan), ilosc_maszyn(p.ilosc_maszyn),
	kolejnosc_zadan(p.kolejnosc_zadan)
{
}

Problem::Ptr Szeregowanie::utworz_podobne()
{
	auto tmp = std::make_unique<Szeregowanie>(*this);
	tmp->mutuj_rozwiazanie();
	return tmp;
}

void Szeregowanie::mutuj_rozwiazanie()
{
	size_t index_a = maszyna_losujaca() % kolejnosc_zadan.size();
	size_t index_b = 0;
	do
	{
		index_b = maszyna_losujaca() % kolejnosc_zadan.size();
	} while (index_a == index_b);
	std::swap(kolejnosc_zadan[index_a], kolejnosc_zadan[index_b]);
	ustal_koszt();
}

void Szeregowanie::ustal_koszt()
{
	std::set<std::pair<int, int>> maszyny; // <moment zakonczenia, id maszyny>
	for (int i = 0; i < ilosc_maszyn; ++i)
		maszyny.insert(std::make_pair(0, i));

	for (const auto &zad_nr : kolejnosc_zadan)
	{
		auto &zadanie = lista_zadan->at(zad_nr);
		if (zadanie.second > ilosc_maszyn)
			throw std::invalid_argument("Zadanie wymaga zbyt duzej ilosc maszyn");

		vec_pair_of_int przydzial;
		przydzial.reserve(zadanie.second);

		auto it_begin = maszyny.begin();
		auto it = maszyny.begin();

		for (int i = 0; i < zadanie.second; ++i)
		{
			przydzial.push_back(*it);
			it++;
		}

		if (przydzial.front().first != przydzial.back().first)
		{
			while (it != maszyny.end())
			{
				if (it->first == przydzial.back().first)
				{
					przydzial.push_back(*it);
					it_begin++;
					it++;
				}
				else
					break;
			}
		}

		for (auto &m : przydzial)
			m.first += zadanie.first;

		maszyny.erase(it_begin, it);
		maszyny.insert(przydzial.end() - zadanie.second, przydzial.end());
	}
	Szeregowanie::koszt = (--maszyny.end())->first;
}

/*
void Szeregowanie::ustal_koszt()
{
	std::set<std::pair<int, int>> maszyny; // <moment zakonczenia, id maszyny>
	for (int i = 0; i < ilosc_maszyn; ++i)
		maszyny.insert(std::make_pair(0, i));

	for (const auto &zad_nr : kolejnosc_zadan)
	{
		auto &zadanie = lista_zadan->at(zad_nr);
		if (zadanie.second > ilosc_maszyn) 
			throw std::invalid_argument("Zadanie wymaga zbyt duzej ilosc maszyn");

		vec_pair_of_int przydzial;
		przydzial.reserve(zadanie.second);

		//szukanie grupy maszyn o takim samym czasie zakonczenia
		//gdzie czas zakonczenia jest najmniejszy
		auto begin_it = maszyny.begin();
		auto end_it = maszyny.begin();
		for (auto it = maszyny.begin(); it != maszyny.end(); ++it)
		{
			if (przydzial.size() == zadanie.second)
				break;

			if (przydzial.size() == 0)
			{
				przydzial.push_back(*it);
				end_it++;
				continue;
			}

			if (it->first != przydzial.back().first)
			{
				przydzial.clear();
				przydzial.push_back(*it);
				begin_it = it;
				end_it = it;
				end_it++;
			}
			else
			{
				przydzial.push_back(*it);
				end_it++;
			}
		}

		if (przydzial.size() == zadanie.second)
		{
			maszyny.erase(begin_it, end_it);
			for (auto &m : przydzial)
			{
				m.first += zadanie.first;
			}
			maszyny.insert(przydzial.begin(), przydzial.end());
		}
		else
		{
			przydzial.clear();
			begin_it = maszyny.begin();
			for (int i = 0; i < (maszyny.size()-zadanie.second); ++i)
			{
				begin_it++;
			}
			for (auto it = begin_it; it != maszyny.end();)
			{
				przydzial.push_back(*it);
				przydzial.back().first += zadanie.first;
				it = maszyny.erase(it);
			}
			maszyny.insert(przydzial.begin(), przydzial.end());
		}
	}
	Szeregowanie::koszt = (--maszyny.end())->first;
}
*/