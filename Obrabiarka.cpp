#include "Obrabiarka.h"
#include <iostream>
#include <algorithm>
#include <limits>

Obrabiarka::Obrabiarka(const Obrabiarka & p):
	rozwiazanie(p.rozwiazanie),ilosc_maszyn(p.ilosc_maszyn),
	ilosc_zadan(p.ilosc_zadan)
{}

Obrabiarka::Obrabiarka(const std::string &filename)
{
	std::fstream wejscie(filename, std::ios_base::in);
	wejscie >> ilosc_zadan;
	wejscie >> ilosc_maszyn;

	rozwiazanie.resize(ilosc_zadan);

	for (auto &vec : rozwiazanie)
	{
		vec.resize(ilosc_maszyn);
		for (auto &val : vec)
		{
			wejscie >> val;
		}
	}

	std::shuffle(rozwiazanie.begin(), rozwiazanie.end(),
		std::mt19937(maszyna_losujaca()));

	koszt = ustal_koszt();
}

void Obrabiarka::mutuj_rozwiazanie()
{
	unsigned index_a = (maszyna_losujaca() % (rozwiazanie.size()));
	unsigned index_b = 0;
	do
	{
		index_b = (maszyna_losujaca() % (rozwiazanie.size()));
	} while (index_a == index_b);
	std::swap(rozwiazanie[index_a], rozwiazanie[index_b]);
	koszt = ustal_koszt();
}
Problem::Ptr Obrabiarka::utworz_podobne()
{
	auto tmp = std::make_unique<Obrabiarka>(*this);
	tmp->mutuj_rozwiazanie();
	return tmp;
}

double Obrabiarka::ustal_koszt()
{
	if (!rozwiazanie.empty())
	{
		std::vector<int> ends;
		ends.resize(ilosc_zadan);

		for (int i = 0; i < ilosc_maszyn; ++i)
		{

			if (i == 0)
			{
				ends[0] = rozwiazanie[0][i];
				for (int j = 1; j < ilosc_zadan; ++j)
				{
					ends[j] = ends[j - 1] + rozwiazanie[j][i];
				}
			}

			else
			{
				ends[0] += rozwiazanie[0][i];
				for (int j = 1; j < ilosc_zadan; ++j)
				{
					if(ends[j] >= ends[j-1])
						ends[j] = ends[j] + rozwiazanie[j][i];
					else
						ends[j] = ends[j-1] + rozwiazanie[j][i];
				}
			}
		}

		return ends.back();
	}
	return std::numeric_limits<double>::max();
}
