#pragma once
#include "Problem.h"
#include <random>
#include <memory>
#include <cmath>

class Wyzarzanie
{
public:
	Wyzarzanie(double p_t, double p_alfa) : t(p_t), alfa(p_alfa) {};
	~Wyzarzanie();
	std::shared_ptr<Problem> run(Problem* ptr);
private:
	const double t;
	const double alfa;
	std::random_device maszyna_losujaca;
};

