#pragma once
#include "Problem.h"
#include <string>
#include <vector>
#include <set>
#include <fstream>
#include <algorithm>
#include <random>
class Szeregowanie :
	public Problem
{
public:
	Szeregowanie(const Szeregowanie &p);
	Szeregowanie(const std::string &filename);
	virtual ~Szeregowanie() override = default;
	virtual Problem::Ptr utworz_podobne() override;
private:
	void ustal_koszt();
	void mutuj_rozwiazanie();
	typedef std::vector<std::pair<int, int>> vec_pair_of_int;
	int ilosc_maszyn;
	std::shared_ptr<vec_pair_of_int> lista_zadan; // <czas trwania, liczba maszyn>
	std::vector<int> kolejnosc_zadan;
	std::random_device maszyna_losujaca;
};


