#include "Wyzarzanie.h"


Wyzarzanie::~Wyzarzanie()
{
}

std::shared_ptr<Problem> Wyzarzanie::run(Problem *ptr)
{
	std::shared_ptr<Problem> obecne_rozwiazanie = ptr->utworz_podobne();
	std::shared_ptr<Problem> best_rozwiazanie = obecne_rozwiazanie;
	double ct = t;
	while (ct > 0.1)
	{
		auto nowe_rozwiazanie = obecne_rozwiazanie->utworz_podobne();
		double d_koszt = nowe_rozwiazanie->podaj_koszt() - obecne_rozwiazanie->podaj_koszt();
		if (d_koszt < 0)
		{
			obecne_rozwiazanie = std::move(nowe_rozwiazanie);
			if (obecne_rozwiazanie->podaj_koszt() < best_rozwiazanie->podaj_koszt())
				best_rozwiazanie = obecne_rozwiazanie;
		}
		else if ((static_cast<double>(maszyna_losujaca()) / maszyna_losujaca.max()) < exp(-d_koszt / ct))
			obecne_rozwiazanie = std::move(nowe_rozwiazanie);
		ct *= alfa;
	}
	return best_rozwiazanie;
}
