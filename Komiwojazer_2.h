#pragma once
#include "Problem.h"
#include <vector>
#include <random>
#include <fstream>

class Komiwojazer_2 :
	public Problem
{
public:
	Komiwojazer_2(const Komiwojazer_2 & p);
	Komiwojazer_2(const std::string & filename);
	virtual ~Komiwojazer_2() override = default;
	virtual Problem::Ptr utworz_podobne() override;
private:
	typedef std::vector<std::vector<int>> int_2d_vec;
	double ustal_koszt();
	void mutuj_rozwiazanie();
	std::random_device maszyna_losujaca;
	int_2d_vec rozwiazanie;
	std::shared_ptr<int_2d_vec> mapa_kosztow;
};

